<?php
/**
 * @file
 * Admin callbacks for the User Prompt module.
 */

/**
 * Pre-search item index page callback.
 *
 * Lists the existing pre-search items in a table with link to edit and delete.
 */
function search_api_solr_elevate_ids_index() {
  $pre_search_items = _search_api_solr_elevate_ids_item_load_all();
  if ($pre_search_items) {
    $header = array('Label', 'Actions');
    $rows = array();
    foreach ($pre_search_items as $pre_search_item) {
      $rows[] = array(
        $pre_search_item->label,
        l('Edit', 'admin/content/pre-search/edit/'.$pre_search_item->id) . ' | ' .
        l('Delete', 'admin/content/pre-search/delete/'.$pre_search_item->id)
      );
    }
    return theme('table', array('header' => $header, 'rows' => $rows));
  } else {
    return array('#markup' => '<p>Currently no pre-search items</p>');
  }
}

/**
 * /**
 * Pre-search item create form callback.
 *
 * A form used for editing the 'basic' details of a pre-search item, i.e. everything
 * but the elevated node list which is handled by nodequeue. If a pre-search item id
 * is passed then the form will be used to edit the item.
 *
 * @param $form
 * @param $form_state
 * @param $psid The id of the pre-search item to edit.
 * @return array
 */
function search_api_solr_elevate_ids_create_form($form, &$form_state, $psid) {

  $views_options = array();

  foreach (_gla_search_get_search_api_views() as $view) {
    $views_options[$view->name] = $view->human_name;
  };

  if ($psid) {
    // we're updating an item
    $pre_search_item = _search_api_solr_elevate_ids_item_load($psid);
    $form_state['pre_search_item'] = $pre_search_item;
  }

  $form = array();
  $form['view_name'] = array(
    '#type' => 'select',
    '#title' => t('View'),
    '#options' => $views_options,
    '#required' => TRUE,
    '#default_value' => $pre_search_item ? $pre_search_item->view_name : '',
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#required' => TRUE,
    '#default_value' => $pre_search_item ? $pre_search_item->label : '',
  );
  $form['terms'] = array(
    '#type' => 'textarea',
    '#title' => t('Terms'),
    '#required' => TRUE,
    '#default_value' => $pre_search_item ? $pre_search_item->terms : '',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit handler for the pre-search item create form.
 *
 * @see search_api_solr_elevate_ids_create_form
 */
function search_api_solr_elevate_ids_create_form_submit($form, &$form_state) {

  $values = $form_state['values'];
  $values['terms'] = trim($values['terms']);
  if (isset($form_state['pre_search_item'])) {
    // we're updating
    $pre_search_item = $form_state['pre_search_item'];
    _search_api_solr_elevate_ids_item_update($pre_search_item->id, $values['label'], $values['terms'], $values['view_name']);
  } else {
    $queue = nodequeue_load_queue_by_name('search_api_solr_elevate_ids');
    if (!$queue) {
      // the master nodequeue does not exist!
      return;
    }
    $subqueue = nodequeue_add_subqueue($queue, $values['label']);
    if (!empty($subqueue->sqid)) {
      $pre_search_item = _search_api_solr_elevate_ids_item_create($values['label'], $subqueue->sqid, $values['terms'], $values['view_name']);
      drupal_goto('admin/content/pre-search/edit/' . $pre_search_item->id);
    } else {
      // something went wrong creating the queue.
    }
  }
}

/**
 * Pre-search item delete callback.
 */
function search_api_solr_elevate_ids_edit_delete_callback($id) {
  $pre_search_item = _search_api_solr_elevate_ids_item_load($id);
  nodequeue_remove_subqueue($pre_search_item->qid);
  _search_api_solr_elevate_ids_item_delete($id);
  drupal_set_message('The pre-search item has been deleted');
  drupal_goto('admin/content/pre-search');
}

/**
 * Pre-search item edit page callback.
 *
 * Embeds the pre-search item create form and nodequeue edit form on a single page.
 *
 * @see search_api_solr_elevate_ids_create_form
 */
function search_api_solr_elevate_ids_edit_page($id) {
  $pre_search_item = _search_api_solr_elevate_ids_item_load($id);
  if ($pre_search_item) {
    $sqid = $pre_search_item->qid;
    module_load_include('inc', 'nodequeue', 'includes/nodequeue.admin');
    $subqueue = nodequeue_load_subqueue($sqid);
    $queue = nodequeue_load($subqueue->qid);

    $nodes = _nodequeue_dragdrop_get_nodes($queue, $subqueue);
    $subqueue_form = drupal_get_form('nodequeue_arrange_subqueue_form_' . $subqueue->sqid, $queue, $nodes, $subqueue);
    $pre_search_form = drupal_get_form('search_api_solr_elevate_ids_create_form', $pre_search_item->id);
    return array('pre_search' => $pre_search_form, 'subqueue' => $subqueue_form);
  }
}

